namespace root.templates.patterns {
  'use strict';
  export const patternEditTemplate: string = `
  <md-dialog>
    <md-dialog-content>
      <div class="content-container">

        <md-input-container layout>
          <label>Pattern name</label>
          <input ng-model="patternEditCtrl.patternName">
        </md-input-container>

        <div class="pattern-exercises-list">
          <p class="gym-grey-label">Pattern exercises</p>
          <div flex="75" class="pattern-exercise" ng-repeat="patternExercise in patternEditCtrl.patternExercises" layout>
            <div flex="75" class="gym-dialog-list-item">
              {{patternExercise.exerciseName}}
              <span ng-show="patternExercise.exerciseComment"> ({{patternExercise.exerciseComment}})</span>
            </div>
            <div flex="10" class="clear-container" ng-click="patternEditCtrl.removeExerciseFromPattern($index)">
              <i class="material-icons gym-clickable">clear</i>
            </div>
          </div>
          <div ng-show="patternEditCtrl.patternExercises.length < 1">There are no exercises, add some!</div>
        </div>

        <p class="gym-grey-label">Add exercise to pattern</p>
        <md-input-container>
          <label>Exercise Name</label>
          <input ng-model="patternEditCtrl.patternExerciseNameInput">
        </md-input-container>

        <md-input-container>
          <label>Exercise Comment</label>
          <input ng-model="patternEditCtrl.patternExerciseCommentInput">
        </md-input-container>

        <div class="md-button" ng-click="patternEditCtrl.addExerciseToPattern()">
          <i class="material-icons">add_circle_outline</i>
        </div>

      </div>

    </md-dialog-content>

    <md-dialog-actions>
      <md-button ng-click="patternEditCtrl.cancelDialog()">CANCEL</md-button>
      <md-button ng-click="patternEditCtrl.confirmPattern()">OK</md-button>
    </md-dialog-actions>

  </md-dialog>
  `;
}
