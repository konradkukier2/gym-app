namespace root.templates.exercises {
  'use strict';
  export const exerciseListTemplate: string = `
  <hr>
    <div div flex="100" class="list-container">

    <div layout layout-align="space-between">
      <span class="gym-header">Data : {{mainCtrl.currentDay.date}}
        <span ng-show="mainCtrl.currentDay.comment"> ({{mainCtrl.currentDay.comment}})</span>
      </span>
      <div layout layout-align="center center">
        <i ng-click="mainCtrl.$state.go('dayList')" class="material-icons gym-nav-icon">home</i>
      </div>
    </div>
    <div class="gym-wide-line gym-space"></div>
    <div>
      <div ng-repeat="exercise in mainCtrl.currentDay.exercises" ng-click="mainCtrl.selectExercise(exercise)"
           class="gym-list-item" layout="row" layout-align="start center">
        <span flex="90">{{exercise.name}}
          <span class="gym-comment" ng-if="exercise.comment">({{exercise.comment}})</span>
        </span>
        <div flex="10" class="md-secondary" ng-click="mainCtrl.deleteExercise($index)">
          <i class="material-icons">clear</i>
        </div>
      </div>
    </div>

    <div ng-if="mainCtrl.currentDay.exercises.length === 0" layout="column" layout-align="center center">
      <p>Brak wprowadzonych ćwiczeń</p>
      <div class="gym-red-button" ng-click="mainCtrl.deleteDay()">Usuń dzień</div>
    </div>
    <div class="gym-wide-line gym-space"></div>

    <div layout="column" class="gym-space gym-add-exercise">
      <input ng-model="mainCtrl.newExercise.name" placeholder="Nazwa ćwiczenia">
      <div class="gym-wide-line"></div>
      <input ng-model="mainCtrl.newExercise.unit" placeholder="Jednostka miary (np. kg)">
      <div class="gym-wide-line"></div>
      <input ng-model="mainCtrl.newExercise.comment" placeholder="Komentarz (opcjonalnie)">
      <div class="gym-wide-line"></div>
      <div class="gym-green-button" ng-click="mainCtrl.addExercise()">Dodaj ćwiczenie</div>
    </div>

  </div>
  `;
}
