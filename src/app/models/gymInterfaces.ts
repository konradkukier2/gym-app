'use strict';

namespace root.models {

  export interface IPattern {
    name: string;
    exerciseList: IPatternExercise[];
  }

  export interface IPatternExercise {
    exerciseName: string;
    exerciseComment: string;
  }

  export interface IDayList {
    days: IDay[];
    patterns: IPattern[];
  }

  export interface IDays {
    days: IDay[];
  }

  export interface IDay {
    date: string;
    comment?: string;
    exercises: IExercise[];
  }

  export interface IExercise {
    name: string;
    comment?: string;
    series: ISerie[];
    unit: string;
  }

  export interface ISerie {
    repeats: number;
    value: number;
    comment? : string;
  }

}
