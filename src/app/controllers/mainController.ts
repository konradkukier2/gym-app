namespace root.controllers {
  'use strict';

  export class MainController {

    public static $inject = ['$scope', '$http', '$state', '$filter', 'gymService', '$mdDialog'];
    private patterns: root.models.IPattern[];
    private dayList: root.models.IDays;
    private currentDay: root.models.IDay;
    private currentExercise: root.models.IExercise;
    private editingSerie: root.models.ISerie;
    private selectedPattern: root.models.IPattern;

    private currentDate: Date;
    private currentMoment: moment.Moment;
    private weekDays: string;
    private daysInMonth: any[];

    private newExercise: root.models.IExercise;
    private newPattern: root.models.IPattern;
    private newPatternExercise: root.models.IPatternExercise;

    constructor(private $scope: ng.IScope,
                private $http: ng.IHttpService,
                private $state: ng.ui.IStateService,
                private $filter: any,
                public gymService: root.services.GymService,
                public $mdDialog: ng.material.IDialogService) {
      this.patterns = this.gymService.getPatterns();
      this.dayList = this.gymService.getDayList();
      this.currentDay = this.gymService.getCurrentDay();
      this.currentExercise = this.gymService.getCurrentExercise();
      this.selectedPattern = this.gymService.getCurrentPattern();
      this.editingSerie = null;

      moment.locale('en', { week: { dow: 1 } }); // start with Monday
      this.currentDate = new Date();
      this.currentMoment = moment(this.currentDate);
      this.weekDays = moment.weekdaysMin(<any>true);
      this.daysInMonth = this.getDaysInMonth();
    }

    // pattern methods

    public addPattern = () => {
      const newPattern: root.models.IPattern = {
        name: this.newPattern.name,
        exerciseList: []
      };
      this.patterns = this.gymService.addPattern(newPattern);
      this.newPattern = <root.models.IPattern>{};
    };

    public addPatternExercise = () => {
      const newPatternExercise: root.models.IPatternExercise = {
        exerciseName: this.newPatternExercise.exerciseName,
        exerciseComment: this.newPatternExercise.exerciseComment
      };
      this.selectedPattern.exerciseList.push(newPatternExercise);
      this.newPatternExercise = <root.models.IPatternExercise>{};
    };

    public editPattern = (pattern: root.models.IPattern) => {
      this.selectedPattern = this.gymService.editPattern(pattern);
      this.$state.go('patternDetails');
    };

    public deletePattern = (index: number) => {
      this.patterns = this.gymService.deletePattern(index);
    };

    public deletePatternExercise = (index: number) => {
      this.selectedPattern.exerciseList.splice(index, 1);
    };


    // serie methods

    public addSerie = () => {
      this.currentExercise = this.gymService.addSerie(this.gymService.newSerie());
    };

    public cloneSerie = (serie: root.models.ISerie) => {
      const clonedSerie: root.models.ISerie = angular.copy(serie);
      this.gymService.addSerie(clonedSerie);
    };

    public addVal = (serie: root.models.ISerie, num: number) => {
      serie.value += num;
    };

    public addRepeats = (serie: root.models.ISerie, num: number) => {
      serie.repeats += num;
    };

    public deleteSerie = (serie: root.models.ISerie) => {
      this.currentExercise = this.gymService.deleteSerie(serie);
      this.confirmEdit();
    };

    private confirmEdit = () => {
      this.editingSerie = null;
    };


    // exercise methods

    public selectExercise = (exercise: root.models.IExercise) => {
      this.gymService.selectExercise(exercise);
      this.$state.go('serieList');
    };

    public addExercise = () => {
      const newExercise: root.models.IExercise = {
        name: this.newExercise.name,
        unit: this.newExercise.unit,
        comment: this.newExercise.comment,
        series: []
      };
      this.currentDay = this.gymService.addExercise(newExercise);
      this.newExercise = <root.models.IExercise>{};
    };

    public deleteExercise = (index: number) => {
      this.gymService.deleteExercise(index);
    };


    // day methods

    public deleteDay = () => {
      this.dayList = this.gymService.deleteDay(this.currentDay);
      this.$state.go('dayList');
    };

    public getDaysInMonth = (): number[] => {
      const days: number = this.currentMoment.daysInMonth();
      let firstDay: number = moment(this.currentMoment).date(1).day();
      // support for monday based week
      if (firstDay === 0) { firstDay = 7; }
      firstDay -= 1;

      let daysArray: number[] = [];
      for (let i: number = 1; i <= (firstDay + days); i++) {
        daysArray.push(i > firstDay ? (i - firstDay) : null);
      }
      const nullsToAdd: number = 7 - daysArray.length % 7;
      for (let j: number = 0; j < nullsToAdd; j++) {
        daysArray.push(null);
      }
      const arrayOfArrays: any[] = [];
      let i: number = 0;
      let max: number = daysArray.length;
      for (i; i < max; i++) {
        if (i % 7 === 0) {
          arrayOfArrays.push(daysArray.slice(i, i + 7));
        }
      }
      return arrayOfArrays;
    };

    public selectCalendarDay = (day: number): void => {
      const newMoment: moment.Moment = this.currentMoment.date(day);
      const dateFormatted: string = newMoment.format('DD/MM/YYYY');
      const dayFound: root.models.IDay = this.dayList.days.filter((cDay: root.models.IDay) => cDay.date === dateFormatted)[0];
      if (dayFound) {
        this.gymService.selectDay(dayFound);
        this.$state.go('exerciseList');
        return;
      }
      this.gymService.createDay(newMoment.toJSON()).then((answer: root.models.IDay) => {
        this.dayList = this.gymService.addDay(answer);
      });
    };

    public isDayTaken = (day: number): boolean => {
      if (!day) { return; }
      const newMoment: moment.Moment = this.currentMoment.date(day);
      const dateFormatted: string = newMoment.format('DD/MM/YYYY');
      return this.dayList.days.some((cDay: root.models.IDay) => cDay.date === dateFormatted);
    };

    public nextMonth = (): void => {
      this.currentMoment.add(1, 'months');
      this.daysInMonth = this.getDaysInMonth();
    };

    public prevMonth = (): void => {
      this.currentMoment.subtract(1, 'months');
      this.daysInMonth = this.getDaysInMonth();
    };


    // persistance methods

    public clear = () => {
      this.gymService.clearDayList();
      this.dayList = this.gymService.getDayList();
    };

    public localLoad = () => {
      const fileLoader: HTMLInputElement = <HTMLInputElement>document.querySelector('#fileLoader');
      const listenToFileChange: EventListener = () => {
        const fileReader: FileReader = new FileReader();
        fileReader.addEventListener('load', () => {
          this.dayList = this.gymService.overwriteDayList(JSON.parse(fileReader.result));
          this.$scope.$apply();
        });
        fileReader.readAsText(fileLoader.files[0]);
        fileLoader.removeEventListener('change', listenToFileChange);
      };
      fileLoader.addEventListener('change', listenToFileChange);
      fileLoader.click();
    };

    public localSave = () => {
      const jsonToSave: string = JSON.stringify(this.dayList);
      const blob: Blob = new Blob([jsonToSave], {type: 'application/json'});
      const url: string  = URL.createObjectURL(blob);

      let a: any = document.createElement('a');
      a.setAttribute('style', 'display: none');
      document.querySelector('.list-container').appendChild(a);
      a.href = url;
      a.download = 'dayList_save_.json';
      a.click();
      URL.revokeObjectURL(url);
    };

  }


}
