namespace root.templates.exercises {
  'use strict';
  export const exerciseEditTemplate: string = `
  <md-dialog>
    <md-dialog-content>
      <div class="input-container" layout="column" layout-align="center center">

        <md-input-container>
          <label>Exercise name*</label>
          <input ng-model="mainEditCtrl.exerciseName">
        </md-input-container>

        <md-input-container>
          <label>Comment</label>
          <input ng-model="mainEditCtrl.exerciseComment">
        </md-input-container>

      </div>

    </md-dialog-content>

    <md-dialog-actions>
      <md-button ng-click="mainEditCtrl.cancelDialog()">CANCEL</md-button>
      <md-button ng-click="mainEditCtrl.confirmExercise()">OK</md-button>
    </md-dialog-actions>

  </md-dialog>
  `;
}
