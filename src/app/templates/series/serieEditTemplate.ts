namespace root.templates.series {
  'use strict';
  export const serieEditTemplate: string = `
  <md-dialog>
    <md-dialog-content>
      <div class="input-container" layout="column" layout-align="center center">

        <md-input-container>
          <label>Powtórzenia</label>
          <input ng-model="mainEditCtrl.repeats">
        </md-input-container>

        <md-input-container>
          <label>Wynik</label>
          <input ng-model="mainEditCtrl.value">
        </md-input-container>

        <md-input-container>
          <label>Jednostka</label>
          <input ng-model="mainEditCtrl.unit">
        </md-input-container>

        <md-input-container>
          <label>Komentarz</label>
          <input ng-model="mainEditCtrl.serieComment">
        </md-input-container>

      </div>

    </md-dialog-content>

    <md-dialog-actions>
      <md-button ng-click="mainEditCtrl.cancelDialog()">CANCEL</md-button>
      <md-button ng-click="mainEditCtrl.confirmSerie()">OK</md-button>
    </md-dialog-actions>

  </md-dialog>
  `;
}
