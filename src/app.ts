$((): void => {
  let app: angular.IModule = angular.module('app', ['ui.router', 'ngMaterial']);
  app.service('gymService', root.services.GymService);
  app.config(['$urlRouterProvider', '$stateProvider', ($urlRouterProvider: ng.ui.IUrlRouterProvider,
                                                       $stateProvider: ng.ui.IStateProvider) => {
    $urlRouterProvider.otherwise('days');

    $stateProvider.state('dayList', {
      url: '/days',
      template: root.templates.days.dayListTemplate,
      controller: root.controllers.MainController,
      controllerAs: 'mainCtrl'
    })
    .state('exerciseList', {
      url: '/exercises',
      template: root.templates.exercises.exerciseListTemplate,
      controller: root.controllers.MainController,
      controllerAs: 'mainCtrl'
    })
    .state('serieList', {
      url: '/series',
      template: root.templates.series.serieListTemplate,
      controller: root.controllers.MainController,
      controllerAs: 'mainCtrl'
    })
    .state('patternList', {
      url: '/patterns',
      template: root.templates.patterns.patternListTemplate,
      controller: root.controllers.MainController,
      controllerAs: 'mainCtrl'
    })
    .state('patternDetails', {
      url: '/patternDetails',
      template: root.templates.patterns.patternDetailsTemplate,
      controller: root.controllers.MainController,
      controllerAs: 'mainCtrl'
    });

  }]);

  app.run(($rootScope: ng.IRootScopeService, $http: ng.IHttpService) => {
    //
  });

  angular.bootstrap(document, ['app']);

});
