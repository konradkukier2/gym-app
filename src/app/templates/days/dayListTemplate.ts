'use strict';
namespace root.templates.days {
  export const dayListTemplate: string = `
<hr>

  <div flex="100" class="list-container">
    <div layout layout-align="space-between">
      <span class="gym-header">Kalendarz dni</span>
      <div layout layout-align="center center">
        <i ng-click="mainCtrl.$state.go('patternList')" class="material-icons gym-nav-icon">color_lens</i>
      </div>
    </div>
    <div class="gym-wide-line gym-space"></div>
    <div>

      <div layout="row" flex="100">
        <div layout="column" layout-align="start center" flex="100">

          <div layout="row" layout-align="space-between center" style="width: 50%">
            <md-button aria-label="prev" class="md-icon-button" ng-click="mainCtrl.prevMonth()">
              <i class="material-icons">keyboard_arrow_left</i>
            </md-button>
            {{mainCtrl.currentMoment.format("MMMM YYYY")}}
            <md-button aria-label="next" class="md-icon-button" ng-click="mainCtrl.nextMonth()">
              <i class="material-icons">keyboard_arrow_right</i>
            </md-button>
          </div>

          <div layout="row" layout-align="space-around center" class="gym-datepicker-weekdays">
            <div layout layout-align="center center" ng-repeat="weekDay in mainCtrl.weekDays track by $index">{{ weekDay }}</div>
          </div>

          <div layout="column" flex="100" style="width: 100%">
            <div layout="row" layout-align="space-around" ng-repeat="array in mainCtrl.daysInMonth track by $index">
              <div layout layout-align="center center" ng-repeat="day in array track by $index">
                <md-button class="gym-datepicker-day" ng-class="{ 'gym-datepicker-day-null': day === null, 'gym-datepicker-day-taken': mainCtrl.isDayTaken(day) === true }"
                            ng-click="mainCtrl.selectCalendarDay(day)" aria-label="day"><strong>{{ day }}</strong>
                </md-button>
              </div>
            </div>
          </div>

        </div>
      </div>

    </div>
    <div class="gym-wide-line gym-space"></div>
    <!--<div layout="row" layout-align="end" class="gym-button gym-with-margin" ng-click="mainCtrl.addDay()">
      <div class="gym-icon-text">New</div>
      <div><i class="material-icons">add_circle_outline</i></div>
    </div>-->
    <md-button ng-click="mainCtrl.clear()">Clear</md-button>
    <md-button ng-click="mainCtrl.localLoad()">Load</md-button>
    <md-button ng-click="mainCtrl.localSave()">Save</md-button>
    <input type="file" id="fileLoader" style="display: none"/>
  </div>
  `;
}
