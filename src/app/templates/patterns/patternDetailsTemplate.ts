namespace root.templates.patterns {
  "use strict";
  export const patternDetailsTemplate: string = `
<hr>

  <div flex="100" class="list-container">
    <div layout layout-align="space-between">
      <div layout layout-align="center center">
        <i ng-click="mainCtrl.$state.go('patternList')" class="material-icons gym-nav-icon">arrow_back</i>
      </div>
      <span class="gym-header">Szablon: {{mainCtrl.selectedPattern.name}}</span>
      <div layout layout-align="center center">
        <i ng-click="mainCtrl.$state.go('dayList')" class="material-icons gym-nav-icon">home</i>
      </div>
    </div>
    <div class="gym-wide-line gym-space"></div>

    <div>
      <div class="gym-list-item standard-cursor" ng-repeat="patternExercise in mainCtrl.selectedPattern.exerciseList"
           layout="row" layout-align="start center">
        <span flex="90">{{patternExercise.exerciseName}}
          <span class="gym-comment" ng-if="patternExercise.exerciseComment">({{patternExercise.exerciseComment}})</span>
        </span>
        <div flex="10" class="gym-delete pointer-cursor" ng-click="mainCtrl.deletePatternExercise($index)">
        <i class="material-icons">clear</i>
        </div>
      </div>
    </div>

    <div ng-if="mainCtrl.selectedPattern.exerciseList.length === 0" layout layout-align="center center">
      <p class="gym-info">Brak wprowadzonych ćwiczeń dla szablonu</p>
    </div>
    <div class="gym-wide-line gym-space"></div>

    <div layout="column" class="gym-space gym-add-exercise">
      <input ng-model="mainCtrl.newPatternExercise.exerciseName" placeholder="Nazwa ćwiczenia">
      <div class="gym-wide-line"></div>
      <input ng-model="mainCtrl.newPatternExercise.exerciseComment" placeholder="Komentarz (opcjonalnie)">
      <div class="gym-wide-line"></div>
      <div class="gym-green-button" ng-click="mainCtrl.addPatternExercise()">Dodaj ćwiczenie</div>
    </div>

  </div>
  `;
}
