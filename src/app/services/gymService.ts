namespace root.services {
  'use strict';

  export class GymService {
      public static $inject = ['$q', '$http', '$state', '$mdDialog'];

      private dayList: root.models.IDayList;
      private dl: string;
      private currentDay: root.models.IDay;
      private currentExercise: root.models.IExercise;
      private editingSerie: root.models.ISerie;
      private currentPattern: root.models.IPattern;
      private emptyPattern: root.models.IPattern;

      constructor(private $q: ng.IQService,
                  private $http: ng.IHttpService,
                  private $state: ng.ui.IStateService,
                  private $mdDialog: ng.material.IDialogService) {
        this.dl = window.localStorage.getItem('dayList');
        this.dayList = JSON.parse(this.dl);
        if (!this.dayList) { this.clearDayList(); }
        this.currentDay = { date: '', exercises: [] };
        this.currentExercise = { name: '', series: [], unit: '' };
        this.editingSerie = { repeats: 0, value: 0 };
        this.emptyPattern = { name: '', exerciseList: [] };
      }

      // pattern methods

      public addPattern = (pattern: root.models.IPattern) => {
        this.dayList.patterns.push(pattern);
        return this.dayList.patterns;
      };

      public editPattern = (pattern: root.models.IPattern) => {
        this.currentPattern = pattern;
        return this.currentPattern;
      };

      public deletePattern = (index: number) => {
        this.dayList.patterns.splice(index, 1);
        return this.dayList.patterns;
      };

      public getPatterns = () => this.dayList.patterns;

      public getCurrentPattern = () => this.currentPattern;


      // serie methods

      public newSerie = () => {
        return this.createEmptySerie();
      };

      private createEmptySerie = () => {
        return {
          repeats: 0,
          value: 0,
          comment: ''
        };
      };

      public addSerie = (serie: root.models.ISerie) => {
        this.currentExercise.series.push(serie);
        return this.currentExercise;
      };

      public deleteSerie = (serie: root.models.ISerie) => {
        for (let i: number = 0; i < this.currentExercise.series.length; i++) {
          if (serie === this.currentExercise.series[i]) {
            this.currentExercise.series.splice(i, 1);
            break;
          }
        }
        return this.currentExercise;
      };


      // exercise methods

      public selectExercise = (exercise: root.models.IExercise): void => {
        this.currentExercise = exercise;
      };

      public addExercise = (exercise: root.models.IExercise) => {
        this.currentDay.exercises.push(exercise);
        return this.currentDay;
      };

      public deleteExercise = (index: number) => {
        this.currentDay.exercises.splice(index, 1);
        return this.currentDay;
      };

      public getCurrentExercise = () => this.currentExercise;


      // day methods

      public createDay = (jsonDate: string) => {
        const locals: Object = {
          date: jsonDate,
          comment: '',
          exercises: [],
          patterns: this.dayList.patterns
        };
        const options: ng.material.IDialogOptions = {
          controller: root.controllers.MainEditController,
          controllerAs: 'mainEditCtrl',
          bindToController: true,
          template: root.templates.days.dayEditTemplate,
          locals: locals
        };
        return this.$mdDialog.show(options);
      };

      public selectDay = (day: root.models.IDay): void => {
        this.currentDay = day;
      };

      public addDay = (day: root.models.IDay) => {
        this.dayList.days.push(day);
        return this.dayList;
      };

      public deleteDay = (dayToDelete: root.models.IDay) => {
        this.dayList.days = this.dayList.days.filter((day: root.models.IDay) => {
          return day.date !== dayToDelete.date;
        });
        return this.dayList;
      };

      public overwriteDayList = (daylist: root.models.IDayList) => {
        return this.dayList = daylist;
      };

      public getDayList = () => this.dayList;

      public clearDayList = () => {
        this.dayList = {
          'days': [],
          'patterns': []
        };
      };

      public getCurrentDay = () => this.currentDay;

  }
}
