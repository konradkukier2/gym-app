namespace root.templates.patterns {
  "use strict";
  export const patternListTemplate: string = `
<hr>

  <div flex="100" class="list-container">
    <div layout layout-align="space-between">
      <span class="gym-header">Szablony</span>
      <div layout layout-align="center center">
        <i ng-click="mainCtrl.$state.go('dayList')" class="material-icons gym-nav-icon">home</i>
      </div>
    </div>
    <div class="gym-wide-line gym-space"></div>
    <div>
      <div class="gym-list-item" ng-repeat="pattern in mainCtrl.patterns" ng-click="mainCtrl.editPattern(pattern)"
           layout="row" layout-align="start center">
        <span flex="90">{{pattern.name}}</span>
        <div flex="10" class="gym-delete" ng-click="mainCtrl.deletePattern($index)">
        <i class="material-icons">clear</i>
        </div>
      </div>
    </div>
    <div ng-if="mainCtrl.patterns.length === 0" layout layout-align="center center">
      <p class="gym-info">Brak wprowadzonych szablonów. Utwórz je aby przyspieszyć proces dodawania dni treningowych</p>
    </div>
    <div class="gym-wide-line gym-space"></div>

    <div layout="column" class="gym-space gym-add-exercise">
      <input ng-model="mainCtrl.newPattern.name" placeholder="Nazwa szablonu">
      <div class="gym-wide-line"></div>
      <div class="gym-green-button" ng-click="mainCtrl.addPattern()">Dodaj szablon</div>
    </div>

  </div>
  `;
}
