namespace root.controllers {
  'use strict';

  export class MainEditController {

    public static $inject = ['$scope', '$mdDialog', '$filter'];

    private repeats: number;
    private value: number;
    private unit: string;
    private serieComment: string;

    private date: string;
    private comment: string;
    private exercises: root.models.IExercise[];
    private choosenPattern: root.models.IPattern;

    private exerciseName: string;
    private exerciseComment: string;
    private series: root.models.ISerie[];

    constructor(private $scope: ng.IScope,
                private $mdDialog: ng.material.IDialogService,
                private $filter: ng.IFilterService) {
      this.date = this.$filter('date')(new Date(this.date), 'dd/MM/yyyy');
    }

    public cancelDialog = (): void => {
      this.$mdDialog.cancel();
    };

    public confirmDay = (): void => {
      //@TODO: remove this controller totally
      if (this.choosenPattern) {
        this.choosenPattern.exerciseList.forEach((pattern: root.models.IPatternExercise) => {
          const exercise: root.models.IExercise = {
            name: pattern.exerciseName,
            comment: pattern.exerciseComment,
            series: [],
            unit: 'kg'
          };
          this.exercises.push(exercise);
        });
      }
      const answer: Object = {
        date: this.date,
        comment: this.comment,
        exercises: this.exercises
      };
      this.$mdDialog.hide(answer);
    };

    public confirmExercise = (): void => {
      if (!this.exerciseName) { return; }
      const answer: Object = {
        name: this.exerciseName,
        comment: this.exerciseComment,
        series: this.series
      };
      this.$mdDialog.hide(answer);
    };

    public confirmSerie = (): void => {
      const answer: Object = {
        repeats: this.repeats,
        value: this.value,
        unit: this.unit,
        comment: this.serieComment
      };
      this.$mdDialog.hide(answer);
    };


  }


}
