namespace root.templates.series {
  'use strict';
  export const serieListTemplate: string = `
  <hr>
    <div div flex="100" class="list-container">

      <div layout layout-align="space-between">
        <div layout layout-align="center center">
          <i ng-click="mainCtrl.$state.go('exerciseList')" class="material-icons gym-nav-icon">arrow_back</i>
        </div>
        <span class="gym-header">{{mainCtrl.currentExercise.name}}</span>
        <div layout layout-align="center center">
          <i ng-click="mainCtrl.$state.go('dayList')" class="material-icons gym-nav-icon">home</i>
        </div>
      </div>
      <div class="gym-wide-line gym-space"></div>

      <md-card class="gym-card" ng-repeat="serie in mainCtrl.currentExercise.series" layout="column" layout-align="center">
        <div layout>
          <div flex="25" layout layout-align="start center">
            <i class="gym-icon-big material-icons" ng-click="mainCtrl.cloneSerie(serie)">get_app</i>
          </div>
          <div flex="50" layout layout-align="center center">Seria #{{$index + 1}}</div>
          <div flex="25" layout layout-align="end center">
            <i class="gym-icon-big material-icons" ng-click="mainCtrl.deleteSerie(serie)">clear</i>
          </div>
        </div>
        <div layout>
          <div flex="15" layout layout-align="start center">
            <i class="material-icons gym-negative icon-flipped" ng-click="mainCtrl.addRepeats(serie, -1)">play_arrow</i>
          </div>
          <div flex="70" layout layout-align="center center">Powtórzenia: {{serie.repeats}}</div>
          <div flex="15" layout layout-align="end center">
            <i class="material-icons gym-positive" ng-click="mainCtrl.addRepeats(serie, 1)">play_arrow</i>
          </div>
        </div>
        <div layout>
          <div flex="25" layout layout-align="start center">
            <i class="material-icons gym-negative" ng-click="mainCtrl.addVal(serie, -10)">fast_rewind</i>
            <i class="material-icons icon-flipped gym-negative" ng-click="mainCtrl.addVal(serie, -1)">play_arrow</i>
          </div>
          <div flex="50" layout layout-align="center center">Wynik: {{serie.value + " " +  mainCtrl.currentExercise.unit}}</div>
          <div flex="25" layout layout-align="end center">
            <i class="material-icons gym-positive" ng-click="mainCtrl.addVal(serie, 1)">play_arrow</i>
            <i class="material-icons gym-positive" ng-click="mainCtrl.addVal(serie, 10)">fast_forward</i>
          </div>
        </div>
          <!--<div class="gym-delete-serie-container" ng-click="mainCtrl.deleteSerie(serie)">
            <i class="gym-icon-big material-icons">delete_forever</i>
          </div>
          <span class="gym-serie-label md-headline">Seria #{{$index + 1}}</span>
          <span class="md-subhead">Powtórzenia: {{serie.repeats}}</span>
          <span class="md-subhead">Wynik: {{serie.value + " " +  serie.unit}}</span>
          <span class="md-subhead" ng-show="serie.comment">Komentarz: {{serie.comment}}</span>-->


      </md-card>

      <div ng-if="mainCtrl.currentExercise.series.length === 0" layout layout-align="center">
        <p>Brak wprowadzonych serii</p>
      </div>
      <div class="gym-wide-line gym-space"></div>

      <div layout="column" class="gym-with-margin">
        <div class="gym-green-button" ng-click="mainCtrl.addSerie()">Dodaj serie</div>
      </div>

  </div>
  `;
}
