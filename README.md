**Gym App 1.0**

1) use `npm install && bower install && gulp` to build

2) after building properly you can run app by setting up local server and running `/src/index.html`

----

Gym App is tool that helps you with recording your results of exercising activites.

It uses Angular (with UI router) + Typescript. UI is powered by Angular Material.

`ver 1.0` is first version that lets you use end to end flow of application, so:
- application is missing a lot of features / improvements
- application is ready to be used