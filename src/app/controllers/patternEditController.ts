namespace root.controllers {
  'use strict';

  export class PatternEditController {

    public static $inject = ['$mdDialog'];

    private patternName: string;
    private patternExercises: root.models.IPatternExercise[];
    private patternExerciseNameInput: string;
    private patternExerciseCommentInput: string;

    constructor(private $mdDialog: ng.material.IDialogService) {
      //
    }

    public cancelDialog = (): void => {
      this.$mdDialog.cancel();
    };

    public confirmPattern = (): void => {
      const answer: Object = {
        name: this.patternName,
        exerciseList: this.patternExercises
      };
      this.$mdDialog.hide(answer);
    };

    public addExerciseToPattern = (): void => {
      if (!this.patternExerciseNameInput) { return; }
      this.patternExercises.push({
        exerciseName: this.patternExerciseNameInput,
        exerciseComment: this.patternExerciseCommentInput
      });
      this.patternExerciseNameInput = '';
      this.patternExerciseCommentInput = '';
    };

    public removeExerciseFromPattern = (index: number): void => {
      this.patternExercises.splice(index, 1);
    };

  }
}
