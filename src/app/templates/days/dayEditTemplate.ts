namespace root.templates.days {
  'use strict';
  export const dayEditTemplate: string = `
  <md-dialog>
    <md-dialog-content>
      <div class="input-container" layout="column" layout-align="center center">

        <md-input-container>
          <label>Date</label>
          <input disabled ng-model="mainEditCtrl.date">
        </md-input-container>

        <md-input-container>
          <label>Comment</label>
          <input ng-model="mainEditCtrl.comment">
        </md-input-container>

        <md-input-container>
          <label>Pattern</label>
          <md-select ng-model="mainEditCtrl.choosenPattern">
            <md-option ng-value="pattern" ng-repeat="pattern in mainEditCtrl.patterns">
              <span>{{pattern.name}}</span>
            </md-option>
            <span class="gym-info" ng-if="mainEditCtrl.patterns.length === 0">Brak szablonów. Możesz je tworzyć z menu głównego</span>

          </md-select>
        </md-input-container>

      </div>

    </md-dialog-content>

    <md-dialog-actions>
      <md-button ng-click="mainEditCtrl.cancelDialog()">CANCEL</md-button>
      <md-button ng-click="mainEditCtrl.confirmDay()">OK</md-button>
    </md-dialog-actions>

  </md-dialog>
  `;
}
